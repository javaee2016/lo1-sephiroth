package se.miun.dsv.javaee16.social.lo1.model;

import java.io.Serializable;
import java.time.*;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;


@Entity
public class Comment implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private long id; // Primary key
	
	@Column(name = "CommentText")
	private String commentText;
	
	@Column(name = "Time")
	private LocalDateTime time;
	
	@ManyToOne // Makes the association bidirectional
	@JoinColumn(name="UserID")  
	private User author;
	
	@ManyToOne // Makes the association bidirectional
	@JoinColumn(name = "event_id")
	private Event event;
	
	@Column(name = "LastUpdate")
	private LocalDateTime lastUpdate;
	
	public Comment(){} // Empty constructor
	
	/**
	 * public constructor
	 * @param commentText The comments text
	 * @param author The author of the comment
	 * @param event the event this comment belongs to
	 */
	public Comment(Event event, User author, String commentText, LocalDateTime timeCreated){
		
		this.commentText = commentText;
		this.author = author;
		this.event = event;
		this.time = timeCreated;
	}
	// always update lastUpdate before Persist
	@PrePersist
	private void prePersist(){
		this.lastUpdate = LocalDateTime.now();
	}
	// always update lastUpdate before update
	@PreUpdate
	private void preUpdate(){
		this.lastUpdate = LocalDateTime.now();
	}
	
	
	// Getters and setters
	
	public String getCommentText(){
		return commentText;
	}
	
	public void setCommentText(String commentText){
		this.commentText = commentText;
	}
	
	public User getAuthor(){
		return author;
	}
	
	public void setAuthor(User author){
		this.author = author;
	}
	
	public LocalDateTime getTime(){
		return time;
	}
	
	public void setTime(LocalDateTime time){
		this.time = time;
	}
	
	public Event getEvent(){
		return event;
	}
	
	public void setEvent(Event event){
		this.event = event;
	}
	
	// A comment is equal if author, event and time is equal.
	@Override
	public boolean equals(Object o){
        if ( this == o ) {
            return true;
        }
        if ( o == null || getClass() != o.getClass() ) {
            return false;
        }
        Comment comment = (Comment) o;
        
        return (author.equals(comment.getAuthor()) && 
        		event.equals(comment.getEvent()) &&
        		time.equals(comment.getTime()));
	}
	
	@Override
	public int hashCode(){
		return Objects.hash(author, event, time);
	}
}
