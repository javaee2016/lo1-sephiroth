package se.miun.dsv.javaee16.social.lo1.parsers;

import java.io.BufferedReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import se.miun.dsv.javaee16.social.lo1.model.Comment;
import se.miun.dsv.javaee16.social.lo1.model.Event;
import se.miun.dsv.javaee16.social.lo1.model.User;

/**
 * Parser for a single comment
 * @author frni1203
 *
 */
public class CommentParser {
	private final static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm");
	public final static Comment nullComment = new Comment();
	private BufferedReader br;
	private boolean done;
	private Set<User> users;
	private Event event;
	
	/**
	 * ctor
	 * @param br BufferedReader the parser can parse comments from
	 * @param users set of users used to matched the comment to its author
	 * @param event the event to relate the comment to
	 */
	public CommentParser(BufferedReader br, Set<User> users, Event event) {
		this.br = br;
		this.users = users;
		this.event = event;
		done = false;
	}
	
	/**
	 * Parses the next Comment. BufferedReader provided in constructor must be at the start of a comment line (or empty line)
	 * Updates referenced event and user-objects to reference this comment
	 * @return the parsed comment (or nullComment if no comments remain for the event)
	 * @throws IOException
	 * @throws ParserException
	 */
	public Comment next() throws IOException, ParserException {
		if(done)
			return nullComment;
		String lineBuf = br.readLine();
		//Detect eof or end of section
		if(lineBuf == null || lineBuf.isEmpty()) {
			done = true;
			return nullComment;
		}
		
		//Comment is on format: \tuseremail (yy/MM/dd HH:mm): "text"
		Pattern commentPattern = Pattern.compile("\\t([^ ]+) \\(((?:[\\d]{2}[\\/ :]?){5})\\)\\: \\\"([^\"]*)\\\"");
		Matcher m = commentPattern.matcher(lineBuf);
		if(!m.find() || m.groupCount() != 3) {
			throw new ParserException("Invalid comment format");
		}
		
		User author = null;
		//Match authors by email
		for(User candidate : users) {
			if(candidate.getEmail().equals(m.group(1))) {
				author = candidate;
				break;
			}
		}
				
		if(author == null) {
			throw new ParserException("Unknown user authored comment");
		}
		return new Comment(event, author, m.group(3), LocalDateTime.parse(m.group(2), dateTimeFormatter));
	}	
}
