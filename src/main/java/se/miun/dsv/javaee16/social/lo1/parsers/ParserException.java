package se.miun.dsv.javaee16.social.lo1.parsers;

/**
 * Exception thrown by parser (contains only stubs)
 * @author frni1203
 *
 */
public class ParserException extends Exception {
	private static final long serialVersionUID = 1L;

	public ParserException() {}

	public ParserException(String arg0) {
		super(arg0);
	}

	public ParserException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ParserException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public ParserException(Throwable arg0) {
		super(arg0);
	}

}
