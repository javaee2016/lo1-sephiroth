package se.miun.dsv.javaee16.social.lo1.parsers;

import java.io.BufferedReader;
import java.io.IOException;

import se.miun.dsv.javaee16.social.lo1.model.User;

/**
 * Parser for a single user
 * @author frni1203
 *
 */
public class UserParser {
	public final static User nullUser = new User();
	private BufferedReader br;
	private boolean done;
	
	/**
	 * ctor
	 * @param br BufferedReader to read users from
	 */
	public UserParser(BufferedReader br) {
		this.br = br;
		done = false;
	}
	
	/**
	 * Parses the next User. BufferedReader provided in constructor must be at the start of a user line 
	 * (or at the start of the user section, or at empty line - the end of the section)
	 * No comments are added to the user by parsing only the user
	 * @return the parsed User (or nullUser if no users remain to be read)
	 * @throws IOException
	 * @throws ParserException
	 */
	public User next() throws ParserException, IOException {
		if(done)
			return nullUser;
		
		String lineBuf = br.readLine();
		//Detect end of section or eof
		if(lineBuf == null || lineBuf.isEmpty()) {
			done = true;
			return nullUser;
		}
		//Eat the header of the users section
		if(lineBuf.equals("Users:"))
			return next();
		
		//Parse user data on the form: Firstname[space]Lastname[tab]E-mail
		String[] userData = lineBuf.split("\\s+", 3);
		if(userData.length != 3)
			throw new ParserException("Invalid number of user tokens");
		return new User(userData[0], userData[1], userData[2]);
	}
}
