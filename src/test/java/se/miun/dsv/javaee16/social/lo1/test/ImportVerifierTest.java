package se.miun.dsv.javaee16.social.lo1.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import se.miun.dsv.javaee16.social.lo1.model.Event;

public class ImportVerifierTest {

	private final IImportVerifier verifier;
	
	public ImportVerifierTest() {
		this.verifier = new ImportVerifier();
	}
	
	@Test
	public void testFindAllEventsThatOverlapWithOthers() {
		List<Event> overlaps = verifier.findAllEventsThatOverlapWithOthers();

		assertEquals(2, overlaps.size());
		
		overlaps.sort((e1,e2) -> e1.getStartTime().compareTo(e2.getStartTime()));

		Event earlierStartEvent = overlaps.get(0);
		Event laterStartEvent = overlaps.get(0);
		
		assertTrue(laterStartEvent.getStartTime().isBefore(earlierStartEvent.getEndTime()));
	}

	@Test
	public void testFindFullNamesOfUsersHostingFutureEvents() {
		List<String> futureHosts = verifier.findFullNamesOfUsersHostingFutureEvents();

		 assertTrue(futureHosts.contains("Robert Jonsson"));
		 assertTrue(futureHosts.contains("Christoffer Fink"));
		 assertTrue(futureHosts.contains("Per Ekeroot"));
		 assertTrue(futureHosts.contains("Felix Dobslaw"));
		 assertTrue(futureHosts.contains("Fredrik Aletind"));
	}

	@Test
	public void testFindNumberOfUsersWithMoreThanThreeComments() {
		long count = verifier.findNumberOfUsersWithMoreThanOneComment();
		assertEquals(3L, count);
	}

	@Test
	public void testFindPastEventsInHamburg() {
		List<Event> events = verifier.findPastEventsInHamburg();
		assertEquals(2, events.size());
		assertEquals("City Cleaning", events.get(0).getTitle());
	}
}