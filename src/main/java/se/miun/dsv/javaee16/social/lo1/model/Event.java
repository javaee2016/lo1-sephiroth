package se.miun.dsv.javaee16.social.lo1.model;

import java.io.Serializable;
import java.time.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@Entity
public class Event implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private long id; // Primary key
	
	@Column(name = "Title") 
	private String title;
	
	@Column(name = "City")
	private String city;
	
	@Column(name = "StartTime")
	private LocalDateTime startTime;
	
	@Column(name ="EndTime")
	private LocalDateTime endTime;
	
	@Column(name = "LastUpdate")
	private LocalDateTime lastUpdate;
	
	@Column(name = "Content")
	private String content;
	
	// Each user can organize many events and each event can have many organizers
	@ManyToMany
	@JoinTable(
			name = "Events_Users", // join table name
			joinColumns = @JoinColumn(name = "Event_id"), // the name of the events column
			inverseJoinColumns = @JoinColumn(name = "User_id") // the name of the users column
	)
	private Set<User> organizers;
	
	@OneToMany(mappedBy="event") // one event can have many comments but one comment can have only one event
	private Set<Comment> comments; // mapped by the event field in the comment class.
	
	public Event(){} // Empty constructor
	
	/**
	 * 
	 * @param title The events title
	 * @param city The city where the event is held
	 * @param content The event description
	 * @param startTime The start time of the event
	 * @param endTime The end time of the event
	 * @param organizers The events organizers
	 */
	public Event(String title, String city, String content, LocalDateTime startTime,
				LocalDateTime endTime, Set<User> organizers){
			
		this.title = title;
		this.city = city;
		this.content = content;
		this.startTime = startTime;
		this.endTime = endTime;
		this.organizers = organizers;
		this.comments = new HashSet<Comment>();
	}
	
	// always update lastUpdate before Persist
	@PrePersist
	private void prePersist(){
		this.lastUpdate = LocalDateTime.now();
	}
	// always update lastUpdate before update
	@PreUpdate
	private void preUpdate(){
		this.lastUpdate = LocalDateTime.now();
	}
	
	/* getters and setters */
	
	public String getTitle(){
		return title;
	}
	
	public void setTitle(String title){
		this.title = title;
	}
	
	public String getCity(){
		return city;
	}
	
	public void setCity(String city){
		this.city = city;
	}
	
	public LocalDateTime getStartTime(){
		return startTime;
	}
	
	public void setStartTime(LocalDateTime startTime){
		this.startTime = startTime;
	}
	
	public LocalDateTime getEndTime(){
		return endTime;
	}
	
	public void setEndTime(LocalDateTime endTime){
		this.endTime = endTime;
	}
	
	public String getContent(){
		return content;
	}
	
	public void setContent(String content){
		this.content = content;
	}
	
	public Set<User> getOrganizers(){
		return organizers;
	}
	
	public void setOrganizers(Set<User> organizers){
		this.organizers = organizers;
	}
	
	public Set<Comment> getComments(){
		return comments;
	}
	
	public void setComments(Set<Comment> comments){
		this.comments = comments;
	}
	
	@Override
	public boolean equals(Object o){
        if ( this == o ) {
            return true;
        }
        if ( o == null || getClass() != o.getClass() ) {
            return false;
        }
        Event Event = (Event) o;
        
        return (title.equals(Event.getTitle()) && 
        		city.equals(Event.getCity()) &&
        		startTime.equals(Event.getStartTime()));
	}
	
	@Override
	public int hashCode(){
		return Objects.hash(title, city, startTime);
	}
}
