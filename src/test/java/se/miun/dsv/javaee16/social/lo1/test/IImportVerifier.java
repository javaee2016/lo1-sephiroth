package se.miun.dsv.javaee16.social.lo1.test;

import java.util.List;

import se.miun.dsv.javaee16.social.lo1.model.Event;

public interface IImportVerifier {
	
	public abstract List<Event> findAllEventsThatOverlapWithOthers();

	public abstract List<String> findFullNamesOfUsersHostingFutureEvents();

	public abstract Long findNumberOfUsersWithMoreThanOneComment();

	public abstract List<Event> findPastEventsInHamburg();
	
}
