package se.miun.dsv.javaee16.social.lo1;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;



public class DatabaseCreator {

	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("social");
	
	public static void main(String[] args) {
		
		emf.close();

	}

}
