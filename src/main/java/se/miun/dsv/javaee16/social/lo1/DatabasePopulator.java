package se.miun.dsv.javaee16.social.lo1;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import se.miun.dsv.javaee16.social.lo1.model.Comment;
import se.miun.dsv.javaee16.social.lo1.model.Event;
import se.miun.dsv.javaee16.social.lo1.model.User;
import se.miun.dsv.javaee16.social.lo1.parsers.EventsStreamParser;
import se.miun.dsv.javaee16.social.lo1.parsers.ParserException;

/**
 * Routine to populate database from events-file
 * @author frni1203
 *
 */
public class DatabasePopulator {
	final private static String defaultUrl = "https://bitbucket.org/javaee2016/javaee16/raw/master/material/events.txt";
	/**
	 * Populate the database
	 * @param args running without arguments fetches a default file, otherwise, use url to events.txt-file as argument
	 */
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("social");
		DatabasePopulator dp = new DatabasePopulator(emf);
		try {
			if(args.length > 1)
				dp.populate(args[1]);
			else 
				dp.populate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(emf != null) {
				emf.close();
			}
		}
	}
	
	private EntityManagerFactory emf;

	public DatabasePopulator(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	public void populate() throws ParserException, IOException {
		populate(defaultUrl);
	}
	
	public void populate(String url) throws ParserException, IOException {
		InputStream is = null;
		try {
			//Parse the graph
			URL inputFileUrl = new URL(url);
			is = inputFileUrl.openStream();
			EventsStreamParser esp = new EventsStreamParser(is);
			esp.parseEvents();
						
			EntityManager em = emf.createEntityManager();
			EntityTransaction tx = em.getTransaction();
			
			try {
				tx.begin();
				//Users
				for(User u : esp.getUsers()) { em.persist(u); }
				//Events
				for(Event e : esp.getEvents()) { em.persist(e); }
				//Comments
				for(Comment c : esp.getComments()) { em.persist(c); }
				tx.commit();
			} catch(Exception e) {
				e.printStackTrace();
				tx.rollback();
				throw e;
			}
		} catch (IOException | ParserException e) {
			throw e;
		} finally {
			if(is != null) {
				try {
					is.close();
				} catch(IOException e) {}
			}
		}
	}
}
