package se.miun.dsv.javaee16.social.lo1.parsers;

import java.io.BufferedReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import se.miun.dsv.javaee16.social.lo1.model.Event;
import se.miun.dsv.javaee16.social.lo1.model.User;

/**
 * Parser for a single event
 * @author frni1203
 *
 */
public class EventParser {
	private final static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm");
	public final static Event nullEvent = new Event();
	private BufferedReader br;
	private boolean done;
	private Set<User> users;
	
	/**
	 * ctor
	 * @param br BufferedReader to parse events from.
	 * @param users set of user objects the parser can use to setup organizer-relationship with
	 */
	public EventParser(BufferedReader br, Set<User> users) {
		this.br = br;
		this.users = users;
		done = false;
	}
	
	
	/**
	 * Parses the next event. BufferedReader provided in constructor must be at the start of Events section, or at the start of an event
	 * no comments are added by simply parsing events
	 * @return the parsed event (or EventParser.nullEvent if no events remain to be parsed)
	 * @throws IOException
	 * @throws ParserException
	 */
	public Event next() throws IOException, ParserException {
		if(done)
			return nullEvent;
		String lineBuf = br.readLine();
		//Detect eof or end of section
		if(lineBuf == null || lineBuf.isEmpty()) {
			done = true;
			return nullEvent;
		}
		//Eat the header
		if(lineBuf.equals("Events:"))
			return next();
		
		//Event is on format: Title, City, Description, Start - Stop, [organizer1, organizer2, ..., organizerN], regex below matches these (but not the organizers)
		Pattern p = Pattern.compile("([^,]+)(?:, )([^,]+)(?:, )([^,]+)(?:, )((?:[^ ]+)(?: )(?:[^ ]+))(?: - )((?:[^ ]+)(?: )(?:[^,]+))");
		Matcher m = p.matcher(lineBuf);
		if(!m.find() || m.groupCount() != 5) {
			throw new ParserException("Invalid event format");
		}
		
		String title = m.group(1);
		String city = m.group(2);
		String description = m.group(3);
		LocalDateTime startTime = LocalDateTime.parse(m.group(4), dateTimeFormatter);
		LocalDateTime stopTime = LocalDateTime.parse(m.group(5), dateTimeFormatter);
		
		//Lame regex to extract something resembling e-mail adresses for organizers from line
		//TODO: Ninja regex that will extract organizers together with the rest of the data
		p = Pattern.compile("(?:\\[)((?:[^\\]])+)");
		m = p.matcher(lineBuf);
		if(!m.find() || m.groupCount() != 1)
			throw new ParserException("Invalid organizer format for event");
		String organizerStrs[] = m.group(1).split(", ");
		Set<User> organizers = new HashSet<User>();
		for(String organizerEmail : organizerStrs) {
			//Match user by email
			for(User candidate : users) {
				if(candidate.getEmail().equals(organizerEmail)) {
					organizers.add(candidate);
					break;
				}
			}
		}
		
		//Were all organizers found?
		if(organizers.size() != organizerStrs.length)
			throw new ParserException("Event contained unknown users");
		
		return new Event(title, city, description, startTime, stopTime, organizers);
	}
}
