package se.miun.dsv.javaee16.social.lo1.test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import se.miun.dsv.javaee16.social.lo1.DatabasePopulator;
import se.miun.dsv.javaee16.social.lo1.model.Event;
import se.miun.dsv.javaee16.social.lo1.parsers.ParserException;

public class ImportVerifier implements IImportVerifier {

	private static EntityManagerFactory emf = null;
	
	public ImportVerifier() {
		//Since JUnit will create a new ImportVerifier for each test case
		//keep the EntityManagerFactory as a static member that only gets initialized once
		//to avoid creating multiple emfs
		//This solution is a bit ugly, and should probably be refactored later (Registry pattern?)
		if(emf == null || !emf.isOpen()) {
			emf = Persistence.createEntityManagerFactory("social");
			
			//For now, populate the db every time
			DatabasePopulator dp = new DatabasePopulator(emf);
			try {
				dp.populate();
			} catch (IOException | ParserException e) {
				if(emf != null) {
					emf.close();
				}
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Event> findAllEventsThatOverlapWithOthers() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("SELECT DISTINCT e1 FROM Event e1, Event e2 WHERE e1.startTime < e2.endTime AND e1.endTime > e2.startTime AND e1 <> e2").getResultList();
	}

	//Uses JPQL
	@SuppressWarnings("unchecked")
	@Override
	public List<String> findFullNamesOfUsersHostingFutureEvents() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("SELECT DISTINCT CONCAT(u.firstName, ' ', u.lastName) FROM Event e, IN (e.organizers) u WHERE e.startTime > :now")
				.setParameter("now", LocalDateTime.now()).getResultList();
	}

	//Uses JPQL
	@Override
	public Long findNumberOfUsersWithMoreThanOneComment() {
		EntityManager em = emf.createEntityManager();
		return new Long((long)em.createQuery("SELECT count(u) FROM User u WHERE size(u.comments) > 1").getSingleResult()); 
	}
	
	//Uses JPQL
	@SuppressWarnings("unchecked")
	@Override
	public List<Event> findPastEventsInHamburg() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("SELECT e FROM Event e WHERE e.endTime < :now AND e.city = 'Hamburg'").setParameter("now", LocalDateTime.now()).getResultList();
	}
}
