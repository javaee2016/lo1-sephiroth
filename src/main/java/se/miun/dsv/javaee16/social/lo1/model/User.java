package se.miun.dsv.javaee16.social.lo1.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Users")
public class User implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private long id; // primary key
	
	@Column(name="FirstName")
	private String firstName;
	
	@Column(name="LastName")
	private String lastName;
	
	@Column(name="email", unique = true)
	private String email;
	
	@OneToMany(mappedBy="author") // one author can have many comments but comment can have only one author
	private Set<Comment> comments; // mapped by the author field in the Comment class
	
	public User(){} // Empty constructor.
	
	/**
	 * Constructor
	 * @param firstname The users first name
	 * @param lastName The users last name
	 */
	public User(String firstName, String lastName, String email){
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.comments = new HashSet<Comment>();
	}
	
	
	/* Getters and setters */
	
	public String getFirstName(){
		return firstName;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public String getEmail(){
		return email;
	}
	
	public void setFirstName(String firstName){
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName){
		this.lastName = lastName;
	}
	
	public void setEmail(String email){
		this.email = email;
	}
	
	// If two users have same email they are equal.
	@Override
	public boolean equals(Object o){
        if ( this == o ) {
            return true;
        }
        if ( o == null || getClass() != o.getClass() ) {
            return false;
        }
        User user = (User) o;
        
        return email.equals(user.getEmail());
	}
	
	@Override
	public int hashCode(){
		return Objects.hashCode(email);
	}
	
}
